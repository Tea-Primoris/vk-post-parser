﻿//------------------------------------------------------------------------------
// <auto-generated>
//     Этот код создан по шаблону.
//
//     Изменения, вносимые в этот файл вручную, могут привести к непредвиденной работе приложения.
//     Изменения, вносимые в этот файл вручную, будут перезаписаны при повторном создании кода.
// </auto-generated>
//------------------------------------------------------------------------------

namespace VKparserService
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Infrastructure;
    
    public partial class vkDBmodelContainer : DbContext
    {
        public vkDBmodelContainer()
            : base("name=vkDBmodelContainer")
        {
        }
    
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            throw new UnintentionalCodeFirstException();
        }
    
        public virtual DbSet<textTable> textTableSet { get; set; }
        public virtual DbSet<imageTable> imageTableSet { get; set; }
        public virtual DbSet<urlTable> urlTableSet { get; set; }
    }
}

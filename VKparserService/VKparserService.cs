﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using System.Timers;
using System.Web.Script.Serialization;
using System.IO;
using System.Data.Entity;

namespace VKparserService
{
    public partial class VKparserService : ServiceBase
    {
        readonly EventLog eventLog1;
        readonly FileSystemWatcher watcher = new FileSystemWatcher("C:/Temp");
        const String PATH2TEXT = "C:/Temp\\text.json";
        const String PATH2IMG = "C:/Temp\\images.json";
        const String PATH2URL = "C:/Temp\\urls.json";

        readonly Mutex TextMutex = new Mutex(false, "TextMutex");
        readonly Mutex ImageMutex = new Mutex(false, "ImageMutex");
        readonly Mutex URLsMutex = new Mutex(false, "URLsMutex");

        readonly vkDBmodelContainer container;

        public VKparserService()
        {
            InitializeComponent();

            eventLog1 = new EventLog();
            if (!EventLog.SourceExists("VK parser"))
            {
                EventLog.CreateEventSource(
                    "VK parser", "VK parser log");
            }
            eventLog1.Source = "VK parser";
            eventLog1.Log = "VK parser log";

            container = new vkDBmodelContainer();
        }

        protected override void OnStart(string[] args)
        {
            eventLog1.WriteEntry("Service started.", EventLogEntryType.Information);
            eventLog1.WriteEntry(PATH2IMG, EventLogEntryType.Information);
            watcher.Changed += Watcher_Changed;
            eventLog1.WriteEntry("Watcher attached.", EventLogEntryType.Information);
            watcher.EnableRaisingEvents = true;
        }

        private void Watcher_Changed(object sender, FileSystemEventArgs e)
        {
            if (e.FullPath.Equals(PATH2TEXT))
            {
                eventLog1.WriteEntry("Waiting for text mutex.", EventLogEntryType.Information);
                TextMutex.WaitOne();
                eventLog1.WriteEntry("Text mutex entered.", EventLogEntryType.SuccessAudit);

                string[] lines = File.ReadAllLines(Path.GetFullPath(e.FullPath));
                foreach (string line in lines)
                {
                    JavaScriptSerializer serializer = new JavaScriptSerializer();
                    TextClass textClass = serializer.Deserialize<TextClass>(line);
                    eventLog1.WriteEntry("Text deserialized.", EventLogEntryType.Information);
                    textTable textTable = new textTable
                    {
                        postID = textClass.postID,
                        postText = textClass.postText
                    };
                    container.textTableSet.Add(textTable);
                }
                container.SaveChanges();
                eventLog1.WriteEntry("Changes to database saved.", EventLogEntryType.Information);

                TextMutex.ReleaseMutex();
                eventLog1.WriteEntry("Text mutex released", EventLogEntryType.Information);
            }
            if (e.FullPath.Equals(PATH2IMG))
            {
                eventLog1.WriteEntry("Waiting for image mutex.", EventLogEntryType.Information);
                ImageMutex.WaitOne();
                eventLog1.WriteEntry("Image mutex entered.", EventLogEntryType.SuccessAudit);

                string[] lines = File.ReadAllLines(Path.GetFullPath(e.FullPath));
                foreach (string line in lines)
                {
                    JavaScriptSerializer serializer = new JavaScriptSerializer();
                    ImageClass imageClass = serializer.Deserialize<ImageClass>(line);
                    eventLog1.WriteEntry("Images deserialized.", EventLogEntryType.Information);
                    imageTable imgTable = new imageTable()
                    {
                        PostID = imageClass.postID,
                        postImage = imageClass.postImages
                    };
                    container.imageTableSet.Add(imgTable);
                }
                container.SaveChanges();
                eventLog1.WriteEntry("Changes to database saved.", EventLogEntryType.Information);

                ImageMutex.ReleaseMutex();
                eventLog1.WriteEntry("Image mutex released", EventLogEntryType.Information);
            }
            if (e.FullPath.Equals(PATH2URL))
            {
                eventLog1.WriteEntry("Waiting for url mutex.", EventLogEntryType.Information);
                URLsMutex.WaitOne();
                eventLog1.WriteEntry("URL mutex entered.", EventLogEntryType.SuccessAudit);

                string[] lines = File.ReadAllLines(Path.GetFullPath(e.FullPath));
                foreach (string line in lines)
                {
                    JavaScriptSerializer serializer = new JavaScriptSerializer();
                    URLClass urlClass = serializer.Deserialize<URLClass>(line);
                    eventLog1.WriteEntry("URL deserialized.", EventLogEntryType.Information);
                    urlTable URLTable = new urlTable
                    {
                        PostId = urlClass.postID,
                        postURL = urlClass.postURLs
                    };
                    container.urlTableSet.Add(URLTable);
                }
                container.SaveChanges();
                eventLog1.WriteEntry("Changes to database saved.", EventLogEntryType.Information);

                TextMutex.ReleaseMutex();
                eventLog1.WriteEntry("Text mutex released", EventLogEntryType.Information);
            }
        }

        protected override void OnStop()
        {
            watcher.EnableRaisingEvents = false;
            eventLog1.WriteEntry("Service stoped.", EventLogEntryType.Information);
        }
    }
}

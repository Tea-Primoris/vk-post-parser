
-- --------------------------------------------------
-- Entity Designer DDL Script for SQL Server 2005, 2008, 2012 and Azure
-- --------------------------------------------------
-- Date Created: 05/16/2019 17:00:03
-- Generated from EDMX file: C:\Users\lvetc\Source\Repos\vk-post-parser\VKparserService\vkDBmodel.edmx
-- --------------------------------------------------

SET QUOTED_IDENTIFIER OFF;
GO
USE [VKDataBase];
GO
IF SCHEMA_ID(N'dbo') IS NULL EXECUTE(N'CREATE SCHEMA [dbo]');
GO

-- --------------------------------------------------
-- Dropping existing FOREIGN KEY constraints
-- --------------------------------------------------


-- --------------------------------------------------
-- Dropping existing tables
-- --------------------------------------------------

IF OBJECT_ID(N'[dbo].[textTableSet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[textTableSet];
GO
IF OBJECT_ID(N'[dbo].[imageTableSet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[imageTableSet];
GO
IF OBJECT_ID(N'[dbo].[urlTableSet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[urlTableSet];
GO

-- --------------------------------------------------
-- Creating all tables
-- --------------------------------------------------

-- Creating table 'textTableSet'
CREATE TABLE [dbo].[textTableSet] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [postID] nvarchar(max)  NOT NULL,
    [postText] nvarchar(max)  NOT NULL
);
GO

-- Creating table 'imageTableSet'
CREATE TABLE [dbo].[imageTableSet] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [PostID] nvarchar(max)  NOT NULL,
    [postImage] nvarchar(max)  NOT NULL
);
GO

-- Creating table 'urlTableSet'
CREATE TABLE [dbo].[urlTableSet] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [PostId] nvarchar(max)  NOT NULL,
    [postURL] nvarchar(max)  NOT NULL
);
GO

-- --------------------------------------------------
-- Creating all PRIMARY KEY constraints
-- --------------------------------------------------

-- Creating primary key on [Id] in table 'textTableSet'
ALTER TABLE [dbo].[textTableSet]
ADD CONSTRAINT [PK_textTableSet]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'imageTableSet'
ALTER TABLE [dbo].[imageTableSet]
ADD CONSTRAINT [PK_imageTableSet]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'urlTableSet'
ALTER TABLE [dbo].[urlTableSet]
ADD CONSTRAINT [PK_urlTableSet]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- --------------------------------------------------
-- Creating all FOREIGN KEY constraints
-- --------------------------------------------------

-- --------------------------------------------------
-- Script has ended
-- --------------------------------------------------
﻿using System;
using System.Runtime.Serialization;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VKparserService
{
    [DataContract]
    class TextClass
    {
        [DataMember]
        public String postID;
        [DataMember]
        public String postText;

        public TextClass()
        {

        }

        public TextClass(String id, String text)
        {
            postID = id;
            postText = text;
        }
    }

    [DataContract]
    class ImageClass
    {
        [DataMember]
        public String postID;
        [DataMember]
        public String postImages;

        public ImageClass()
        {

        }

        public ImageClass(String id, String images)
        {
            postID = id;
            postImages = images;
        }
    }

    [DataContract]
    class URLClass
    {
        [DataMember]
        public String postID;
        [DataMember]
        public String postURLs;

        public URLClass()
        {

        }

        public URLClass(String id, String urls)
        {
            postID = id;
            postURLs = urls;
        }
    }
}

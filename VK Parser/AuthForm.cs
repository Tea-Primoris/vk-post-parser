﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;

namespace VK_Parser
{
    public partial class AuthForm : Form
    {
        private IWebDriver authDriver;

        public AuthForm(IWebDriver driver)
        {
            InitializeComponent();
            authDriver = driver;
        }

        private void AuthButton_Click(object sender, EventArgs e)
        {
            authDriver.FindElement(By.Id("authcheck_code")).SendKeys(authBox.Text);
            authDriver.FindElement(By.Id("login_authcheck_submit_btn")).Click();
            this.Close();
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using OpenQA.Selenium;
using OpenQA.Selenium.Support;
using OpenQA.Selenium.Chrome;
using System.Runtime.Serialization.Json;
using System.IO;
using System.Web.Script.Serialization;

namespace VK_Parser
{
    public partial class MainForm : Form
    {
        readonly IWebDriver driver = new ChromeDriver();
        readonly Mutex TextMutex = new Mutex(false, "TextMutex");    
        readonly Mutex ImageMutex = new Mutex(false, "ImageMutex");
        readonly Mutex URLsMutex = new Mutex(false, "URLsMutex");
        const String LOGIN_URL = "https://vk.com/";
        const String AUTH_URL = "https://vk.com/login?act=authcheck";
        const String FEED_URL = "https://vk.com/feed";
        const String PATH2TEXT = "C://Temp/text.json";         
        const String PATH2IMAGES = "C://Temp/images.json";    
        const String PATH2URLS = "C://Temp/urls.json";        

        public MainForm()
        {
            InitializeComponent();
        }

        private void MainForm_Load(object sender, EventArgs e)
        {
            driver.Navigate().GoToUrl(LOGIN_URL);
        }

        private void MainForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            driver.Quit();
        }

        private void LoginButton_Click(object sender, EventArgs e)
        {
            if (!driver.Url.Equals(LOGIN_URL)) { driver.Navigate().GoToUrl(LOGIN_URL); }
            driver.FindElement(By.Id("index_email")).SendKeys(loginBox.Text);
            driver.FindElement(By.Id("index_pass")).SendKeys(passwordBox.Text);
            driver.FindElement(By.Id("index_login_button")).Click();
            if (driver.Url.Equals(AUTH_URL)) 
            {
                AuthForm authForm = new AuthForm(driver);
                authForm.ShowDialog();
            }
        }

        private void ParserButton_Click(object sender, EventArgs e)
        {
            if (!driver.Url.Equals(FEED_URL)) { MessageBox.Show("Сначала войдите в аккаунт"); }
            ICollection<IWebElement> posts = driver.FindElements(By.XPath("//*[contains(@id, 'post-')]"));
            if (posts.Any())
            {
                Thread textThread = new Thread(() => SerializeText(posts));
                Thread imagesThread = new Thread(() => SerializeImages(posts));
                Thread URLsThread = new Thread(() => SerializeURLs(posts));
                textThread.Start();
                imagesThread.Start();
                URLsThread.Start();
            }
        }

        private void SerializeText(ICollection<IWebElement> posts)
        {

            foreach (IWebElement post in posts)
            {
                string ID = post.GetAttribute("data-post-id");
                ICollection<IWebElement> texts = post.FindElements(By.ClassName("wall_post_text"));
                if (texts.Any())
                {
                    foreach (IWebElement text in texts)
                    {
                        TextClass textClass = new TextClass(ID, text.Text);

                        TextMutex.WaitOne();
                        if (IfDouble<TextClass>(PATH2TEXT, textClass))
                        {
                            Serialize2File<TextClass>(PATH2TEXT, textClass);
                        }
                        TextMutex.ReleaseMutex();
                    }
                }
            }
        }

        private void SerializeImages(ICollection<IWebElement> posts)
        {
            foreach (IWebElement post in posts)
            {
                string ID = post.GetAttribute("data-post-id");
                ICollection<IWebElement> images = post.FindElements(By.XPath("//*[contains(@class, 'image_cover')]"));
                if (images.Any())
                {
                    foreach (IWebElement image in images)
                    {
                        String imageURL = image.GetAttribute("style");
                        imageURL = imageURL.Remove(imageURL.Length - 3).Remove(0, imageURL.IndexOf("url(")).Remove(0, 5);
                        ImageClass imageClass = new ImageClass(ID, imageURL);

                        ImageMutex.WaitOne();
                        if (IfDouble<ImageClass>(PATH2IMAGES, imageClass))
                        {
                            Serialize2File<ImageClass>(PATH2IMAGES, imageClass);
                        }
                        ImageMutex.ReleaseMutex();
                    }
                }
            }
        }

        private void SerializeURLs(ICollection<IWebElement> posts)
        {
            foreach (IWebElement post in posts)
            {
                string ID = post.GetAttribute("data-post-id");
                ICollection<IWebElement> medias = post.FindElements(By.ClassName("media_desc"));
                if (medias.Any())
                {
                    foreach (IWebElement media in medias)
                    {
                        ICollection<IWebElement> urls = media.FindElements(By.ClassName("lnk"));
                        foreach (IWebElement url in urls)
                        {
                            String str = url.GetAttribute("href");
                            URLClass uRLClass = new URLClass(ID, str);

                            URLsMutex.WaitOne();
                            if (IfDouble<URLClass>(PATH2URLS, uRLClass))
                            {
                                Serialize2File<URLClass>(PATH2URLS, uRLClass);
                            }
                            URLsMutex.ReleaseMutex();
                        }
                    }
                }
            }
        }

        private void Serialize2File<T>(String path, T obj)
        {
            JavaScriptSerializer serializer = new JavaScriptSerializer(); //Сериализатор из System.Web.Script.Serialization
            string json = serializer.Serialize(obj);
            using (StreamWriter sw = File.AppendText(path))
            {
                sw.WriteLine(json);
            }
        }

        private bool IfDouble<T>(String path, T obj)
        {
            bool noDoubles = true;
            string[] lines = File.ReadAllLines(path);
            foreach (string line in lines)
            {
                JavaScriptSerializer serializer = new JavaScriptSerializer();
                T obj2 = serializer.Deserialize<T>(line);
                if (obj.Equals(obj2)) noDoubles = false;
            }

            return noDoubles;
        }
    }
}

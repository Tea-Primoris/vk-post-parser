﻿using System;
using System.Runtime.Serialization;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VK_Parser
{
    [DataContract]
    class TextClass
    {
        [DataMember]
        public String postID;
        [DataMember]
        public String postText;
        
        public TextClass()
        {

        }

        public TextClass(String id, String text)
        {
            postID = id;
            postText = text;
        }

        public override bool Equals(Object obj)
        {
            if (obj == null)
            {
                return false;
            }
            else
            {
                TextClass txt = (TextClass)obj;
                return (postID == txt.postID) && (postText == txt.postText);
            }
        }
    }

    [DataContract]
    class ImageClass
    {
        [DataMember]
        public String postID;
        [DataMember]
        public String postImages;

        public ImageClass()
        {

        }

        public ImageClass(String id, String images)
        {
            postID = id;
            postImages = images;
        }
        public override bool Equals(Object obj)
        {
            if (obj == null)
            {
                return false;
            }
            else
            {
                ImageClass img = (ImageClass)obj;
                return (postID == img.postID) && (postImages == img.postImages);
            }
        }
    }

    [DataContract]
    class URLClass
    {
        [DataMember]
        public String postID;
        [DataMember]
        public String postURLs;

        public URLClass()
        {

        }

        public URLClass(String id, String urls)
        {
            postID = id;
            postURLs = urls;
        }

        public override bool Equals(Object obj)
        {
            if (obj == null)
            {
                return false;
            }
            else
            {
                URLClass uRL = (URLClass)obj;
                return (postID == uRL.postID) && (postURLs == uRL.postURLs);
            }
        }
    }
}

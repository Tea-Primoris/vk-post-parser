﻿namespace VK_Parser
{
    partial class AuthForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.authButton = new System.Windows.Forms.Button();
            this.authBox = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // authButton
            // 
            this.authButton.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.authButton.Font = new System.Drawing.Font("Verdana", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.authButton.Location = new System.Drawing.Point(12, 51);
            this.authButton.Name = "authButton";
            this.authButton.Size = new System.Drawing.Size(143, 40);
            this.authButton.TabIndex = 0;
            this.authButton.Text = "Ввести";
            this.authButton.UseVisualStyleBackColor = true;
            this.authButton.Click += new System.EventHandler(this.AuthButton_Click);
            // 
            // authBox
            // 
            this.authBox.Font = new System.Drawing.Font("Verdana", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.authBox.Location = new System.Drawing.Point(12, 12);
            this.authBox.MaxLength = 6;
            this.authBox.Name = "authBox";
            this.authBox.Size = new System.Drawing.Size(143, 33);
            this.authBox.TabIndex = 1;
            this.authBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // AuthForm
            // 
            this.AcceptButton = this.authButton;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(167, 103);
            this.Controls.Add(this.authBox);
            this.Controls.Add(this.authButton);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "AuthForm";
            this.ShowIcon = false;
            this.Text = "Enter auth code";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        public System.Windows.Forms.Button authButton;
        public System.Windows.Forms.TextBox authBox;
    }
}